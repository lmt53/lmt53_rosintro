#!/usr/bin/bash

rosservice call turtle1/teleport_absolute 2.0 10.0 0.0
rosservice call clear

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, -7.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[3.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
