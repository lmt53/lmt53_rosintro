#!/usr/bin/bash

rosservice call turtle1/teleport_absolute 1.0 10.0 0.0
rosservice call clear
rosservice call kill turtle2

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[0.0, -7.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist '[3.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

rosservice call spawn 5.0 10.0 0.0 turtle2
rosservice call turtle2/set_pen 124 252 0 4 off
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, -5.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, -6.0, 0.0]' '[0.0, 0.0, 3.115]'
rostopic pub -1 /turtle2/cmd_vel geometry_msgs/Twist '[0.0, -5.0, 0.0]' '[0.0, 0.0, 0.0]'


