# Python 2/3 compatibility imports
from __future__ import print_function
from six.moves import input

import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg

try:
    from math import pi, tau, dist, fabs, cos
except:  # For Python 2 compatibility
    from math import pi, fabs, cos, sqrt

    tau = 2.0 * pi

    def dist(p, q):
        return sqrt(sum((p_i - q_i) ** 2.0 for p_i, q_i in zip(p, q)))


from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node("move_group_python_interface_tutorial", anonymous=True)

robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()

group_name = "manipulator"
move_group = moveit_commander.MoveGroupCommander(group_name)

display_trajectory_publisher = rospy.Publisher(
    "/move_group/display_planned_path",
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print("============ Planning frame: %s" % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print("============ End effector link: %s" % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print("============ Available Planning Groups:", robot.get_group_names())

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print("============ Printing robot state")
print(robot.get_current_state())
print("")

# --------------
# We get the joint values from the group and change some of the values:
joint_goal = move_group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = - tau / 4
joint_goal[2] = - tau / 8
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = 0  
# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
move_group.go(joint_goal, wait=True)
# Calling ``stop()`` ensures that there is no residual movement
move_group.stop()
# --------------

#Function to set end-effector to different pose goals
def set_pose_goal(my_w, my_x, my_y, my_z):
    pose_goal = geometry_msgs.msg.Pose()
    pose_goal.orientation.w = my_w
    pose_goal.position.x = my_x
    pose_goal.position.y = my_y
    pose_goal.position.z = my_z
    move_group.set_pose_target(pose_goal)
    success = move_group.go(wait=True)
    move_group.stop()
    move_group.clear_pose_targets()

#Beginning of L
set_pose_goal(1.0, 0.0, 0.5, 0.75)
print('Beginning of L')
print(move_group.get_current_joint_values()) #print state at beginning of L
print('')

#move down (-0.3 in z)
set_pose_goal(1.0, 0.0, 0.5, 0.45)

#move accross (-0.2 in y)
set_pose_goal(1.0, 0.0, 0.3, 0.45)

#Beginning of M
set_pose_goal(1.0, 0.0, 0.5, 0.45)
print('Beginning of M')
print(move_group.get_current_joint_values()) #print state at beginning of M
print('')

#move up (+0.3 in z)
set_pose_goal(1.0, 0.0, 0.5, 0.75)

#move across (-0.1 in y)
set_pose_goal(1.0, 0.0, 0.4, 0.75)

#move down (-0.1 in z)
set_pose_goal(1.0, 0.0, 0.4, 0.65)

#move up (+0.1 in z)
set_pose_goal(1.0, 0.0, 0.4, 0.75)

#move across (-0.1 in y)
set_pose_goal(1.0, 0.0, 0.3, 0.75)

#move down (-0.3 in z)
set_pose_goal(1.0, 0.0, 0.3, 0.45)

#Beginning of T
set_pose_goal(1.0, 0.0, 0.5, 0.75)
print('Beginning of T')
print(move_group.get_current_joint_values()) #print state at beginning of T
print('')
#move accross (-0.1 in y)
set_pose_goal(1.0, 0.0, 0.4, 0.75)

#move down (-0.3 in z)
set_pose_goal(1.0, 0.0, 0.4, 0.45)

#move up (+0.3 in z)
set_pose_goal(1.0, 0.0, 0.4, 0.75)

#move accross (-0.1 in y)
set_pose_goal(1.0, 0.0, 0.3, 0.75)